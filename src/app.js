const express = require('express')
const bodyParser = require("body-parser")
const cors = require("cors")

require('./libs/passport')

const productsRoute = require("./routes/productsRoute")

const app = express()
const PORT = process.env.PORT || 5000

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/products', productsRoute)

app.listen(PORT, () => {
    console.log(`Server listening on PORT: ${PORT}`)
})

module.exports = app
