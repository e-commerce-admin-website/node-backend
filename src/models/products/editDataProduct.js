// Libs 
const MongoConnection = require("../../libs/mongoConnection")
const mongodb = require("mongodb")

const handleEditDataProduct = async (req, res) => {

    const json = await updateOneProduct(req.body)

    if (!json) {
        return res.status(400).json({
            success: false,
            error_message: 'cannot edit'
        })
    }

    res.status(200).json({
        success: true
    })

}

const updateOneProduct = async (query) => {
    return await MongoConnection(async (db) => {

        const {
            products_id,
            products_name,
            products_image,
            products_price,
            products_quantity,
            products_category,
            products_description,
        } = query

        const where = {
            _id: mongodb.ObjectID(products_id)
        }

        const update = {
            $set: {
                products_name,
                products_image,
                products_image,
                products_price,
                products_quantity,
                products_category,
                products_description
            }
        }

        const updated = await db.collection('products').updateOne(where, update)

        return updated

    })
}

module.exports = handleEditDataProduct