// Libs 
const MongoConnection = require("../../libs/mongoConnection")
const mongodb = require("mongodb")

const handleDeleteProduct = async (req, res) => {

    const { id: _id } = req.params

    const json = await deleteOneProduct(_id)

    if (!json) {
        return res.status(400).json({
            success: false,
            error_message: 'cannot delete'
        })
    }

    res.status(200).json({
        success: true
    })

}

const deleteOneProduct = async (_id) => {
    return await MongoConnection(async (db) => {

        const where = {
            _id: mongodb.ObjectID(_id)
        }

        let deleted = await db.collection('products').deleteOne(where)

        return deleted

    })
}

module.exports = handleDeleteProduct
