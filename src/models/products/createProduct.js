// Libs 
const MongoConnection = require("../../libs/mongoConnection")

const handleCreateProduct = async (req, res) => {

    let json = await insertOneProduct(req.body)

    if (json.length < 0) {
        return res.status(400).json({
            success: false,
            error_message: 'cannot insert ...'
        })
    }

    for (let item of json) {

        res.status(200).json({
            success: true,
            data: {
                products_id: item._id,
                products_name: item.products_name,
                products_image: item.products_image,
                products_price: item.products_price,
                products_quantity: item.products_quantity,
                products_category: item.products_category,
                products_description: item.products_description,
                products_created_date: item.products_created_date
            }
        })

        break
    }

}

const insertOneProduct = async (query) => {
    return await MongoConnection(async (db) => {

        const {
            products_name,
            products_image,
            products_price,
            products_quantity,
            products_category,
            products_description,
        } = query

        const document = {
            products_name,
            products_image,
            products_price,
            products_quantity,
            products_category,
            products_description,
            products_created_date: new Date()
        }

        const inserted = await db.collection('products').insertOne(document)

        const resultInserted = inserted.ops

        return resultInserted

    })
}

module.exports = handleCreateProduct