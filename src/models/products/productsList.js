// Libs 
const MongoConnection = require("../../libs/mongoConnection")

const handleProductsList = async (req, res) => {

    let json = await findAllProducts()

    json = json.length > 0 ? json : []

    return res.status(200).json({
        success: true,
        data: json
    })

}

const findAllProducts = async () => {
    return await MongoConnection(async (db) => {

        let dataProductsList = await db.collection('products').find({}).sort({ products_created_date: -1 }).toArray()

        dataProductsList = dataProductsList.length > 0 ? dataProductsList : []

        dataProductsList = dataProductsList.length > 0 && dataProductsList.map(item => {
            return {
                products_id: item._id,
                products_name: item.products_name,
                products_image: item.products_image,
                products_price: item.products_price,
                products_quantity: item.products_quantity,
                products_category: item.products_category,
                products_description: item.products_description,
                products_created_date: item.products_created_date
            }
        })

        return dataProductsList

    })
}

module.exports = handleProductsList