// Libs 
const MongoConnection = require("../../libs/mongoConnection")
const mongodb = require("mongodb")

const handleFindProductByID = async (req, res) => {

    const { id: _id } = req.params

    const json = await findOne(_id)

    if (json === null) {
        res.status(400).json({
            success: false,
            error_message: 'cannot find'
        })
    }

    return res.status(200).json({
        success: true,
        data: json
    })

}

const findOne = async (_id) => {
    return await MongoConnection(async (db) => {

        const where = {
            _id: mongodb.ObjectID(_id)
        }

        let product = await db.collection('products').findOne(where)

        product = {
            products_id: product._id,
            products_name: product.products_name,
            products_image: product.products_image,
            products_price: product.products_price,
            products_quantity: product.products_quantity,
            products_category: product.products_category,
            products_description: product.products_description,
            products_created_date: product.products_created_date
        }

        return product

    })
}

module.exports = handleFindProductByID
