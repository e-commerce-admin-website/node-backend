const express = require('express')

const { authenticate } = require("../middlewares")

const handleProductsList = require("../models/products/productsList")
const handleFindProductByID = require("../models/products/findProductByID")
const handleCreateProduct = require("../models/products/createProduct")
const handleEditDataProduct = require("../models/products/editDataProduct")
const handleDeleteProduct = require("../models/products/deleteProduct")

const router = express.Router()

router.get('/', authenticate, handleProductsList)

router.get('/:id', authenticate, handleFindProductByID)

router.post('/create', authenticate, handleCreateProduct)

router.put('/edit', authenticate, handleEditDataProduct)

router.delete('/:id', authenticate, handleDeleteProduct)

module.exports = router