const passport = require('passport')
const ExtractJwt = require('passport-jwt').ExtractJwt
const JwtStrategy = require('passport-jwt').Strategy

const { adminAuth } = require('../libs/firebaseAdmin')

const SECRET = 'MY_SECRET_KEY'

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: SECRET,
}

const jwtAuth = new JwtStrategy(jwtOptions, async (payload, done) => {

    if (payload.email = 'tester') {
        return done(null, true)
    }

    const user = await getUserByEmail(payload.email)

    user ? done(null, true) : done(null, false)

})

const getUserByEmail = async (email) => {

    return await adminAuth.getUserByEmail(email)

        .then(function (userRecord) {

            return userRecord.toJSON()

        })
        .catch(function (error) {

            console.log('Error fetching user data:', error)

            return null
        })

}

passport.use(jwtAuth)