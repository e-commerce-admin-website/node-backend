var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json")

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://e-commerce-admin-website.firebaseio.com"
})

const adminAuth = admin.auth()

module.exports = {
    adminAuth
}